import { Injectable } from '@angular/core';
import { SimpleDataItem } from '../interfaces';
import { clientGroups } from '../mock-data/client';

@Injectable({
  providedIn: 'root'
})
export class ClientGroupService {

  constructor() { }
  
  getClientGroups(): SimpleDataItem[]{
    return clientGroups
  }

  getClientGroupById(id: number): SimpleDataItem | undefined{
    return clientGroups.find(clientGroup => clientGroup.id == id)
  }

}
