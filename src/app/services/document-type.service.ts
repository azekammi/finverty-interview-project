import { Injectable } from '@angular/core';
import { SimpleDataItem } from '../interfaces';
import { documentTypes } from '../mock-data/identity';

@Injectable({
  providedIn: 'root'
})
export class DocumentTypeService {

  constructor() { }
  
  getDocumentTypes(): SimpleDataItem[]{
    return documentTypes
  }

  getDocumentTypeById(id: number): SimpleDataItem | undefined{
    return documentTypes.find(documentType => documentType.id == id)
  }

}
