import { Injectable } from '@angular/core';
import { SimpleDataItem } from '../interfaces';
import { coordinators } from '../mock-data/client';

@Injectable({
  providedIn: 'root'
})
export class CoordinatorService {

  constructor() { }
  
  getCoordinators(): SimpleDataItem[]{
    return coordinators
  }

  getCoordinatorById(id: number): SimpleDataItem | undefined{
    return coordinators.find(coordinator => coordinator.id == id)
  }

}
