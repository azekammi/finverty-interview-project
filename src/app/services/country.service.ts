import { Injectable } from '@angular/core';
import { Country, SimpleDataItem } from '../interfaces';
import { countries } from '../mock-data/address';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor() { }
  
  getCountries(): Country[]{
    return countries
  }

  getCountryById(id: number): SimpleDataItem | undefined{
    return countries.find(country => country.id == id)
  }

}
