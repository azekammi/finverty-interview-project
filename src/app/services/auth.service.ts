import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from '../interfaces/auth';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient
  ) { }

  login(data: Login){
    return this.http.post(`${environment.BASE_API_URL}/login`, data)
  }

  logout(){
    localStorage.removeItem("auth-test-jwt")
  }

}
