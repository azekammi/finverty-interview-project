import { Injectable } from '@angular/core';
import { SimpleDataItem } from '../interfaces';
import { countries } from '../mock-data/address';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor() { }
  
  getCitiesByCountryId(id: number): SimpleDataItem[]{
    return countries.find(country => country.id == id)?.cities ?? []
  }

  getCityById(id: number): SimpleDataItem | undefined{
    const filteredCities = countries
                            .map(country => country.cities.find(city => city.id == id))
                            .filter(city => !!city);
                                
    return filteredCities.length > 0 ? filteredCities[0] : undefined
  }

}
