import { Injectable } from '@angular/core';
import { SimpleDataItem } from '../interfaces';
import { genders } from '../mock-data/client';

@Injectable({
  providedIn: 'root'
})
export class GenderService {

  constructor() { }
  
  getGenders(): SimpleDataItem[]{
    return genders
  }

  getGenderById(id: number): SimpleDataItem | undefined{
    return genders.find(gender => gender.id == id)
  }

}
