import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import { CLIENT_FORM_KEY } from './client-form/client-form.selectors';
import { IClientForm } from './client-form/client-form.state';
import { clientFormReducer } from './client-form/client-form.reducer';
import { ClientFormEffects } from './client-form/client-form.effects';

export interface State {
  [CLIENT_FORM_KEY]: IClientForm;
}

export const reducers: ActionReducerMap<State> = {
  [CLIENT_FORM_KEY]: clientFormReducer
};

export const metaReducers: MetaReducer<State>[] = !environment.production
  ? []
  : [];

export const effects = [ClientFormEffects];
