import { createReducer, on } from '@ngrx/store';
import { initialClientFormState, IClientForm } from './client-form.state';
import { addAddress, addClient, addIdentity, resetClientFormStore } from './client-form.actions';
import { Client } from 'src/app/interfaces/client';
import { Address } from 'src/app/interfaces/address';
import { Identity } from 'src/app/interfaces/identity';

export const clientFormReducer = createReducer(
  initialClientFormState,
  
  on(addClient, (state: IClientForm, client: Client) => ({
    ...state,
    client
  })),
  on(addAddress, (state: IClientForm, address: Address) => ({
    ...state,
    address
  })),
  on(addIdentity, (state: IClientForm, identity: Identity) => ({
    ...state,
    identity
  })),
  on(resetClientFormStore, (state: IClientForm) => ({
    ...initialClientFormState
  })))