import { Address } from 'src/app/interfaces/address';
import { Client } from 'src/app/interfaces/client';
import { Identity } from 'src/app/interfaces/identity';

export interface IClientForm{
  client?: Client,
  address?: Address,
  identity?: Identity
}

export const initialClientFormState: IClientForm = {
}