import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IClientForm } from './client-form.state';

export const CLIENT_FORM_KEY = "client-form"

export const featureSelector
  = createFeatureSelector<IClientForm>(CLIENT_FORM_KEY);
  
export const clientSelector = createSelector(
  featureSelector,
  state => state.client
);
  
export const addressSelector = createSelector(
  featureSelector,
  state => state.address
);

export const identitySelector = createSelector(
  featureSelector,
  state => state.identity
);