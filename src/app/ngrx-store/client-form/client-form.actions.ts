import { createAction, props } from '@ngrx/store';
import { Address } from 'src/app/interfaces/address';
import { Client } from 'src/app/interfaces/client';
import { Identity } from 'src/app/interfaces/identity';

export const addClient = createAction('[CLIENT_FORM] add client', props<Client>())
export const addAddress = createAction('[CLIENT_FORM] add address', props<Address>())
export const addIdentity = createAction('[CLIENT_FORM] add identity', props<Identity>())

export const resetClientFormStore = createAction('[CLIENT_FORM] clear clientForms')