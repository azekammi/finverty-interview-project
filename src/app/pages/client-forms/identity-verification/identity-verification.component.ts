import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { AutoUnsubscribe, takeWhileAlive } from "take-while-alive";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { addAddress, addIdentity } from "src/app/ngrx-store/client-form/client-form.actions";
import { CountryService } from "src/app/services/country.service";
import { CityService } from "src/app/services/city.service";
import {
  addressSelector,
  clientSelector,
} from "src/app/ngrx-store/client-form/client-form.selectors";
import { switchMap } from "rxjs/operators";
import { DocumentTypeService } from "src/app/services/document-type.service";
import { SettingsService } from "src/app/services/settings.service";

@Component({
  selector: "app-identity-verification",
  templateUrl: "./identity-verification.component.html",
  styleUrls: ["./identity-verification.component.sass"],
})
@AutoUnsubscribe()
export class IdentityVerificationComponent implements OnInit {
  formGroup!: FormGroup;

  documentTypes = this.documentTypeService.getDocumentTypes();

  client$ = this.store.select(clientSelector);
  address$ = this.store.select(addressSelector);

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private documentTypeService: DocumentTypeService,
    private router: Router,
    private settingsService: SettingsService
  ) {}

  ngOnInit(): void {
    this.initSelectors();
    this.createFormGroup();
  }

  createFormGroup() {
    this.formGroup = this.formBuilder.group({
      documentType: new FormControl(null, [Validators.required]),
      series: new FormControl(null),
      number: new FormControl(null, [Validators.required]),
      issuedBy: new FormControl(null),
      dateOfIssue: new FormControl(null, [Validators.required]),
      file: new FormControl(null)
    });
  }

  initSelectors() {
    this.client$
      .pipe(
        takeWhileAlive(this),
        switchMap((client) => {
          if (!client) this.router.navigate(["/client-form/client"]);

          return this.address$.pipe(takeWhileAlive(this));
        })
      )
      .subscribe((address) => {
        if (!address) this.router.navigate(["/client-form/address"]);
      });
  }

  onFileChange(event: Event){
    const input = event.target as HTMLInputElement

    if(input.files && input.files.length > 0){
      this.formGroup.patchValue({
        file: input.files[0]
      })
    }
    else{
      this.settingsService.appComponent.openDialog("Error", "Your browser doesn't support files")
    }
  }

  onSaveClick() {
    console.log(this.formGroup);
    if (!this.formGroup.invalid) {
      this.store.dispatch(addIdentity(this.formGroup.value));

      this.settingsService.appComponent.openDialog("Success", "Client successfully created!", "success")

      this.router.navigate(["/created-client"]);
    }
  }
}
