import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { addClient } from 'src/app/ngrx-store/client-form/client-form.actions';
import { GenderService } from 'src/app/services/gender.service';
import { CoordinatorService } from 'src/app/services/coordinator.service';
import { ClientGroupService } from 'src/app/services/client-group.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.sass']
})
export class ClientComponent implements OnInit {

  formGroup!: FormGroup

  genders = this.genderService.getGenders();
  clientGroups = this.clientGroupService.getClientGroups();
  coordinators = this.coordinatorService.getCoordinators();

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private genderService: GenderService,
    private clientGroupService: ClientGroupService,
    private coordinatorService: CoordinatorService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createFormGroup()
  }

  createFormGroup(){
    this.formGroup = this.formBuilder.group({
      lastName: new FormControl('', [Validators.required]),
      name: new FormControl('', [Validators.required]),
      middleName: new FormControl(null),
      dateOfBirth: new FormControl('', [Validators.required]),
      phoneNumber: new FormControl('', [Validators.required, Validators.pattern("^[0-9]*$"), Validators.minLength(11), Validators.maxLength(11)]),
      gender: new FormControl(null),
      clientGroup: new FormControl([], [Validators.required]),
      coordinator: new FormControl(null),
      doNotSendSMS: new FormControl(false),
    })
  }

  onSaveClick(){
    console.log(this.formGroup)
    if(!this.formGroup.invalid){
      this.store.dispatch(addClient(this.formGroup.value))

      this.router.navigate(["/client-form/address"])
    }
  }

}
