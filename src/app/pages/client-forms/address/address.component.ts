import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from "@angular/forms";
import { AutoUnsubscribe, takeWhileAlive } from "take-while-alive";
import { Router } from "@angular/router";
import { Store } from "@ngrx/store";
import { addAddress } from "src/app/ngrx-store/client-form/client-form.actions";
import { CountryService } from "src/app/services/country.service";
import { CityService } from "src/app/services/city.service";
import { clientSelector } from "src/app/ngrx-store/client-form/client-form.selectors";

@Component({
  selector: "app-add",
  templateUrl: "./address.component.html",
  styleUrls: ["./address.component.sass"],
})
@AutoUnsubscribe()
export class AddressComponent implements OnInit {
  formGroup!: FormGroup;

  countries = this.countryService.getCountries();

  client$ = this.store.select(clientSelector);

  constructor(
    private formBuilder: FormBuilder,
    private store: Store,
    private countryService: CountryService,
    private cityService: CityService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initSelectors()
    this.createFormGroup();
  }

  createFormGroup() {
    this.formGroup = this.formBuilder.group({
      index: new FormControl(null),
      country: new FormControl(null, [Validators.required]),
      area: new FormControl(null),
      city: new FormControl(null, [Validators.required]),
      street: new FormControl(null),
      house: new FormControl(null),
    });
  }

  initSelectors() {
    this.client$.pipe(takeWhileAlive(this)).subscribe((client) => {
      if (!client) this.router.navigate(["/client-form/client"]);
    });
  }

  getCitiesByCountryId() {
    if (!this.formGroup.value.country) return [];

    return this.cityService.getCitiesByCountryId(this.formGroup.value.country);
  }

  onSaveClick() {
    console.log(this.formGroup);
    if (!this.formGroup.invalid) {
      this.store.dispatch(addAddress(this.formGroup.value));

      this.router.navigate(["/client-form/identity"]);
    }
  }
}
