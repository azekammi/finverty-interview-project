import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { SettingsService } from 'src/app/services/settings.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  formGroup!: FormGroup

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private settingsService: SettingsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createFormGroup()
  }

  createFormGroup(){
    this.formGroup = this.formBuilder.group({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    })
  }

  loginButtonClick(){
    if(!this.formGroup.invalid){
      let data = {
        username: this.formGroup.get("username")?.value,
        password: this.formGroup.get("password")?.value
      }

      this.settingsService.increaseLoader()
      this.authService.login(data)
        .subscribe((response: any) => {
          this.settingsService.decreaseLoader()
          if(response.status == "OK"){
            localStorage.setItem("auth-test-jwt", "1")
            this.router.navigate(["/dashboard"])
          }
        })
    }
  }

}
