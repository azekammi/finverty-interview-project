import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { GenderService } from 'src/app/services/gender.service';
import { CoordinatorService } from 'src/app/services/coordinator.service';
import { ClientGroupService } from 'src/app/services/client-group.service';
import { addressSelector, clientSelector, identitySelector } from 'src/app/ngrx-store/client-form/client-form.selectors';
import { CountryService } from 'src/app/services/country.service';
import { CityService } from 'src/app/services/city.service';
import { DocumentTypeService } from 'src/app/services/document-type.service';
import { AutoUnsubscribe, takeWhileAlive } from 'take-while-alive';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-client',
  templateUrl: './created-client.component.html',
  styleUrls: ['./created-client.component.sass']
})
@AutoUnsubscribe()
export class CreatedClientComponent implements OnInit {

  client$ = this.store.select(clientSelector);
  address$ = this.store.select(addressSelector);
  identity$ = this.store.select(identitySelector);

  constructor(
    private store: Store,
    private router: Router,
    private genderService: GenderService,
    private clientGroupService: ClientGroupService,
    private coordinatorService: CoordinatorService,
    private countryService: CountryService,
    private cityService: CityService,
    private documentTypeService: DocumentTypeService
  ) { }

  ngOnInit(): void {
    this.initSelectors()
  }

  initSelectors(){
    this.client$
      .pipe(
        takeWhileAlive(this),
        switchMap((client) => {
          if (!client) this.router.navigate(["/client-form/client"]);

          return this.address$.pipe(takeWhileAlive(this));
        }),
        switchMap((address) => {
          if (!address) this.router.navigate(["/client-form/address"]);

          return this.identity$.pipe(takeWhileAlive(this));
        })
      )
      .subscribe((identity) => {
        if (!identity) this.router.navigate(["/client-form/identity"]);
      });
  }

  //client
  getGenderLabelById(id: number){
    return this.genderService.getGenderById(id)?.label
  }

  getClientGroupLabelsListByIds(ids: number[]): (string | undefined)[]{
    return ids.map(id => this.clientGroupService.getClientGroupById(id)?.label)
  }

  getCoordinatorLabelById(id: number){
    return this.coordinatorService.getCoordinatorById(id)?.label
  }

  //address
  getCountryLabelById(id: number){
    return this.countryService.getCountryById(id)?.label
  }

  getCityLabelById(id: number){
    return this.cityService.getCityById(id)?.label
  }

  //identity
  getDocumentTypeLabelById(id: number){
    return this.documentTypeService.getDocumentTypeById(id)?.label
  }

}
