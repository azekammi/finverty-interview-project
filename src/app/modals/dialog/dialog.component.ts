import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.sass']
})
export class DialogComponent implements OnInit {

  title: string = ""
  description: string = ""
  buttonColor = "danger"
  hasCancel: boolean = false
  buttonText: string = "OK"

  constructor() { }

  ngOnInit(): void {
  }

}
