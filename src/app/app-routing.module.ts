import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './layouts/main/main.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ClientComponent} from './pages/client-forms/client/client.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { LoginComponent } from './pages/login/login.component';
import { IsAuthGuard } from './guards/is-auth.guard';
import { IsNotAuthGuard } from './guards/is-not-auth.guard';
import { AddressComponent } from './pages/client-forms/address/address.component';
import { IdentityVerificationComponent } from './pages/client-forms/identity-verification/identity-verification.component';
import { CreatedClientComponent } from './pages/created-client/created-client.component';

const routes: Routes = [
  {path: '', component: AuthComponent, canActivate: [IsNotAuthGuard], children: [
    {path: '', redirectTo: "login", pathMatch: "full"},
    {path: 'login', component: LoginComponent}
  ]},

  {path: '', component: MainComponent, canActivate: [IsAuthGuard], children: [
    {path: '', redirectTo: "dashboard", pathMatch: "full"},
    {path: 'dashboard', component: DashboardComponent},
    {path: 'client-form/client', component: ClientComponent},
    {path: 'client-form/address', component: AddressComponent},
    {path: 'client-form/identity', component: IdentityVerificationComponent},
    {path: 'created-client', component: CreatedClientComponent},
    {path: '**', redirectTo: "dashboard"},
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
