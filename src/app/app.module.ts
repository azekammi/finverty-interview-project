import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { LAYOUTS, PAGES, COMPONENTS, MODALS } from "./index";

import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { environment } from "../environments/environment";
import { EffectsModule } from "@ngrx/effects";
import { StoreRouterConnectingModule } from "@ngrx/router-store";

import { MatMenuModule } from "@angular/material/menu";
import { MatButtonModule } from "@angular/material/button";
import { MatTableModule } from "@angular/material/table";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule } from "@angular/material/core";
import { MatDialogModule } from "@angular/material/dialog";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { MatFileUploadModule } from 'angular-material-fileupload';

import { AuthService } from "./services/auth.service";
import { ErrorCatchingInterceptor } from "./interceptors/error-catching.interceptor";
import { effects, reducers, metaReducers } from "./ngrx-store";
import { GenderService } from "./services/gender.service";
import { ClientGroupService } from "./services/client-group.service";
import { CoordinatorService } from "./services/coordinator.service";
import { DocumentTypeService } from "./services/document-type.service";
import { CountryService } from "./services/country.service";
import { CityService } from "./services/city.service";

@NgModule({
  declarations: [AppComponent, LAYOUTS, PAGES, COMPONENTS, MODALS],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpClientModule,

    MatMenuModule,
    MatButtonModule,
    MatTableModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatSlideToggleModule,
    MatFileUploadModule,

    StoreModule.forRoot(reducers, {
      metaReducers,
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot(effects),
    StoreRouterConnectingModule.forRoot(),
  ],
  providers: [
    AuthService,
    GenderService,
    ClientGroupService,
    CoordinatorService,
    CountryService,
    CityService,
    DocumentTypeService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorCatchingInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
