import { SimpleDataItem } from "../interfaces";

export const documentTypes: SimpleDataItem[] = [
  {
    id: 1,
    label: "Passport"
  },
  {
    id: 2,
    label: "Birth Certificate"
  },
  {
    id: 3,
    label: "Driving license"
  }
]