import { Gender, ClientGroups } from "../enums/client";
import { SimpleDataItem } from "../interfaces";

export const genders: SimpleDataItem[] = [
  {
    id: Gender.MALE,
    label: "Male",
  },
  {
    id: Gender.FEMALE,
    label: "Female",
  },
];

export const clientGroups: SimpleDataItem[] = [
  {
    id: ClientGroups.VIP_CLIENTS,
    label: "VIP clients",
  },
  {
    id: ClientGroups.LOYAL_CLIENTS,
    label: "Loyal clients",
  },
  {
    id: ClientGroups.NEW_CLIENTS,
    label: "New clients",
  },
];

export const coordinators: SimpleDataItem[] = [
  {
    id: 1,
    label: "Jhones",
  },
  {
    id: 2,
    label: "Colinwood",
  },
  {
    id: 3,
    label: "Doe",
  },
];
