import { Country } from "../interfaces";

export const countries: Country[] = [
  {
    id: 1,
    label: "United Kingdom",
    cities: [
      {
        id: 1,
        label: "London"
      },
      {
        id: 2,
        label: "Liverpool"
      },
    ]
  },
  {
    id: 2,
    label: "Ukraine",
    cities: [
      {
        id: 3,
        label: "Kyiv"
      },
      {
        id: 4,
        label: "Lviv"
      },
    ]
  },
  {
    id: 3,
    label: "Australia",
    cities: [
      {
        id: 5,
        label: "Sydney"
      },
      {
        id: 6,
        label: "Canberra"
      },
    ]
  },
];