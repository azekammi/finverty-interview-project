import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {catchError} from "rxjs/operators";
import { SettingsService } from '../services/settings.service';

@Injectable()
export class ErrorCatchingInterceptor implements HttpInterceptor {

  constructor(
    private settingsService: SettingsService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
            .pipe(
                catchError((error: HttpErrorResponse) => {
                    this.settingsService.decreaseLoader()
                    let errorMsg = '';
                    console.log(error.error)
                    if (error.error instanceof ErrorEvent) {
                        console.log('This is client side error');
                        errorMsg = `Error: ${error.error.message}`;
                    } else {
                        console.log('This is server side error');
                        errorMsg = error.error.error;
                    }
                    this.settingsService.appComponent?.openDialog("Error", errorMsg)
                    return throwError(errorMsg);
                })
            )
  }
}
