import { MainComponent } from "./layouts/main/main.component";
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { DialogComponent } from './modals/dialog/dialog.component';
import { LoaderComponent } from './components/loader/loader.component';
import { AuthComponent } from './layouts/auth/auth.component';
import { LoginComponent } from './pages/login/login.component';
import { ClientComponent } from './pages/client-forms/client/client.component';
import { AddressComponent } from "./pages/client-forms/address/address.component";
import { IdentityVerificationComponent } from "./pages/client-forms/identity-verification/identity-verification.component";
import { CreatedClientComponent } from "./pages/created-client/created-client.component";

export const LAYOUTS = [
  MainComponent,
  AuthComponent
];

export const PAGES = [
  DashboardComponent,
  ClientComponent,
  AddressComponent,
  IdentityVerificationComponent,
  CreatedClientComponent,
  LoginComponent
]

export const COMPONENTS = [
  HeaderComponent,
  LoaderComponent
]

export const MODALS = [
  DialogComponent
]