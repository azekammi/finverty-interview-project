export enum Gender {
    MALE = 1,
    FEMALE = 2
}

export enum ClientGroups {
    VIP_CLIENTS = 1,
    LOYAL_CLIENTS = 2,
    NEW_CLIENTS = 3,
}
