export enum DocumentType {
    PASSPORT = 1,
    BIRTH_CERTIFICATE = 2,
    DRIVING_LICENSE = 3
}