import { Gender, ClientGroups } from '../enums/client';

export interface Client {
    lastName: string,
    name: string,
    middleName?: string,
    dateOfBirth: string,
    phoneNumber: number,
    gender?: Gender,
    clientGroup: ClientGroups[],
    coordinator?: number,
    doNotSendSMS: boolean,
}
