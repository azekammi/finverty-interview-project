export interface SimpleDataItem {
    id: number,
    label: string,
}

export interface Country extends SimpleDataItem {
    cities: SimpleDataItem[]
}