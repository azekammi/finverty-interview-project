import { DocumentType } from "../enums/identity";

export interface Identity {
    documentType: DocumentType,
    series: string,
    number: string,
    issuedBy: string,
    dateOfIssue: string,
    file: File,
}
