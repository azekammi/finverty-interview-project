// Use Express
var express = require("express");
// Use body-parser
var bodyParser = require("body-parser");

// Create new instance of the express server
var app = express();

// Define the JSON parser as a default way 
// to consume and produce data through the 
// exposed APIs
app.use(bodyParser.json());

// Create link to Angular build directory
// The `ng build` command will save the result
// under the `dist` folder.
var distDir = __dirname + "/dist/";
app.use(express.static(distDir));

// Local port.
const LOCAL_PORT = 8080;

var server = app.listen(process.env.PORT || LOCAL_PORT, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
});

//AUTH
app.post("/api/login", function (req, res) {
    var login = req.body;

    if (!login.username) {
        manageError(res, "Invalid employee input", "Username is mandatory.", 400);
    }if (!login.password) {
        manageError(res, "Invalid employee input", "Password is mandatory.", 400);
    } else {
        if(login.username == "admin" && login.password == "admin"){
            res.status(200).json({
                status: "OK"
            });
        }
        else{
            manageError(res, "Username or password is incorrect.", "Username or password is incorrect.", 401);
        }
    }
});

app.get("/api/status", function (req, res) {
    res.status(200).json({ status: "UP" });
});


// Errors handler.
function manageError(res, reason, message, code) {
    console.log("Error: " + reason);
    res.status(code || 500).json({ "error": message });
}
